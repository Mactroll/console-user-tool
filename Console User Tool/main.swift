//
//  main.swift
//  Console User Tool
//
//  Created by Joel Rennich on 3/24/16.
//  Copyright © 2016 Joel Rennich. All rights reserved.
//

import Foundation
import SystemConfiguration

// functions

func usage() {
    print("Simple command line tool to get the local console user.\n\n" +
    "Useage: userutil [option]\n" +
    "-h, help\n" +
    "-n, return the current console user name\n" +
    "-u, return the current consule user uid\n" +
    "-g, return the current console user group\n" +
    "-f, return the current user's full name\n" +
    "-a, return everything\n\n" +
    "Description:\n" +
    "This tool leverages the SCDynamicStore to determine the current console user.\n" +
    "This method should be much more consistent than grepping logs and using \"who.\"\n\n" +
    "Default behavior is -n")
}


// set defaults

var uid: uid_t = 0
var gid: gid_t = 0
var userName: CFString = ""
var errorMessage: String = ""
let fullName = NSFullUserName()

// use SCDynamicStore to find out who the console user is

let theResult = SCDynamicStoreCopyConsoleUser( nil, &uid, &gid)

userName = theResult!


// Get Options from the CLI


do {
    for i in 1 ..< Process.arguments.count {
        switch Process.arguments[i] {
        case "-h":
            usage()
        case "-n":
            print("\(userName)")
        case "-u":
            print("\(uid)")
        case "-g":
            print("\(gid)")
        case "-f":
            print("\(fullName)")
        case "-a":
            print("name = \(userName), uid = \(uid), gid = \(gid) full name= \(fullName)")
        default:
            print("\(userName)")
        }
    }
}

if Process.arguments.count < 2 {
     print("\(userName)")
}

exit(0)
